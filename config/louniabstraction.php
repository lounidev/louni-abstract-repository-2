<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Application Name
    |--------------------------------------------------------------------------
    |
    | This value is the name of your application. This value is used when the
    | framework needs to place the application's name in a notification or
    | any other location as required by the application or its packages.
    */

    'hashid' => [
        'encrypt' => env('HASH_ENCRYPT', true),
        'salt' => env('HASH_SALT', 'random'),
        'length' => env('HASH_LENGTH', 10),
    ],
];
