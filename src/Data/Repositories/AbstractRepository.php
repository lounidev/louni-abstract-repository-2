<?php

namespace Louni\Data\Repositories;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Route;

use Louni\Data\Contracts\RepositoryContract;
use Louni\Helpers\Helper;

use Carbon\Carbon;
use DB;

/**
 * @author Louni <sm.alikamal@hotmail.com>
 */
abstract class AbstractRepository implements RepositoryContract {

    public $path = '';

    protected $routes = [
        'mobile'
    ];

    protected $cacheTag = false;

    protected $builder;
    /**
     *
     * This method will create a new model
     * and will return output back to client as json
     *
     * @access public
     * @return mixed
     *
     * @author Louni <sm.alikamal@hotmail.com>
     *
     **/

    public function create(array $data = [], $encode = true) {

        $newInstance = $this->model->newInstance();
        foreach ($data as $column => $value) {
            $newInstance->{$column} = $value;
        }
        $newInstance->created_at = Carbon::now();
        $newInstance->updated_at = Carbon::now();
        if ($newInstance->save()) {
            // $this->cache()->forget($this->_cacheTotalKey);
            $this->forgetAllCache('', $this->_cacheTotalKey);
            $id = $newInstance->id;
            if($encode) {
                $id = Helper::hashEncode($newInstance->id);
            }
            return $this->findById($id, true, false, $encode);
        } else {
            return false;
        }
    }

    /**
     *
     * This method will fetch single model
     * and will return output back to client as json
     *
     * @access public
     * @return mixed
     *
     * @author Louni <sm.alikamal@hotmail.com>
     *
     **/
    public function findById($id, $refresh = false, $details = false, $encode = true) {

        $this->setCacheKey();

        $data = $this->cache()->get($this->_cacheKey.$id);

        if ($data == NULL || $refresh == true) {

            if($encode) {
                $id = Helper::hashDecode($id);
            }

            $query = $this->model->find($id);
            if ($query != NULL) {

                $data = new \stdClass;
                foreach ($query->getAttributes() as $column => $value) {
                    $data->{$column} = $query->{$column};
                }
                if($encode) {
                    foreach ($data as $key => $value) {
                        $model_hash_keys = !empty($this->model->hash)? $this->model->hash : [];
                        if(in_array($key, $model_hash_keys) || $key === 'id') {
                            $data->{$key} = Helper::hashEncode($value);
                        }
                    }
                    $id =  Helper::hashEncode($id);
                }

                $this->cache()->forever($this->_cacheKey.$id, $data);
            } else {
                return null;
            }
        }
        return $data;
    }

    /**
     *
     * This method will fetch single model by attribute
     * and will return output back to client as json
     *
     * @access public
     * @return mixed
     *
     * @author Louni <sm.alikamal@hotmail.com>
     *
     **/
    public function findByAttribute($attribute, $value, $refresh = false, $details = false, $encode = true) {
        $model = $this->model->newInstance()
                        ->where($attribute, '=', $value)->first(['id']);

        if ($model != NULL) {
            $id = $model->id;
            if($encode) {
                $id = Helper::hashEncode($model->id);
            }
            $model = $this->findById($id, $refresh, $details, $encode);
        }
        return $model;
    }

    /**
     *
     * This method will fetch random model
     * and will return output back to client as json
     *
     * @access public
     * @return mixed
     *
     * @author Louni <sm.alikamal@hotmail.com>
     *
     **/
    public function findRandom($refresh = false, $details = false, $encode = true) {
        $model = $this->model->newInstance()
                        ->inRandomOrder()->first();

        if ($model != NULL) {
            $id = $model->id;
            if($encode) {
                $id = Helper::hashEncode($model->id);
            }
            $model = $this->findById($id, $refresh, $details, $encode);
        }
        return $model;
    }

    /**
     *
     * This method will fetch all exsiting models
     * and will return output back to client as json
     *
     * @access public
     * @return mixed
     *
     * @author Louni <sm.alikamal@hotmail.com>
     *
     **/
    public function findByAll($pagination = false, $perPage = 10, array $input = [], $encode = true ) {
        $ids = $this->builder;

        if ($pagination == true || $this->model->getConnection()->getName() == 'mongodb') {

            $ids = $ids->paginate($perPage);
            $models = $ids->items();

        } else {
            $sql = $ids->toSql();
            $binds = $ids->getBindings();
            $models = DB::select($sql, $binds);
        }
        $data = ['data'=>[]];
        if ($models) {
            foreach ($models as &$model) {
                $id = $model->id;
                if($encode) {
                    $id = Helper::hashEncode($model->id);
                }
                $model = $this->findById($id, !empty($input['refresh']), !empty($input['details']), $encode);
                if ($model) {
                    $data['data'][] = $model;
                }
            }
        }

        if ($pagination == true) {
            $data['pagination'] = [];
            $data['pagination']['total'] = $ids->total();
            $data['pagination']['current'] = $ids->currentPage();
            $data['pagination']['first'] = 1;
            $data['pagination']['last'] = $ids->lastPage();
            $data['pagination']['from'] = $ids->firstItem();
            $data['pagination']['to'] = $ids->lastItem();
            if($ids->hasMorePages()) {
                if ( $ids->currentPage() == 1) {
                    $data['pagination']['previous'] = -1;
                } else {
                    $data['pagination']['previous'] = $ids->currentPage()-1;
                }
                $data['pagination']['next'] = $ids->currentPage()+1;
            } else {
                $data['pagination']['previous'] = $ids->currentPage()-1;
                $data['pagination']['next'] =  $ids->lastPage();
            }
            if ($ids->lastPage() > 1) {
                $data['pagination']['pages'] = range(1,$ids->lastPage());
            } else {
                $data['pagination']['pages'] = [1];
            }
        }
        return $data;
    }

    /**
     *
     * This method will update an existing model
     * and will return output back to client as json
     *
     * @access public
     * @return mixed
     *
     * @author Louni <sm.alikamal@hotmail.com>
     *
     **/
    public function update(array $data = [], $encode = true) {

        $hashId = $data['id'];
        if($encode) {
            $data['id'] = Helper::hashDecode($data['id']);
        }
        
        $model = $this->model->find($data['id']);
        if ($model != NULL) {
            foreach ($data as $column => $value) {
                $model->{$column} = $value;
            }
            $model->updated_at = Carbon::now();

            if ($model->save()) {
                if($encode) {
                    $this->forgetAllCache($this->_cacheKey.$hashId, $this->_cacheTotalKey);
                    return $this->findById($hashId, true, false, $encode);
                }else {
                    $this->forgetAllCache($this->_cacheKey.$data['id'], $this->_cacheTotalKey);
                    return $this->findById($data['id'], true, false, $encode);   
                }
            }
            return false;
        }
        return NULL;
    }

    /**
     *
     * This method will remove model
     * and will return output back to client as json
     *
     * @access public
     * @return bool
     *
     * @author Louni <sm.alikamal@hotmail.com>
     *
     **/
    public function deleteById($id, $encode = true) {
        if($encode) {
            $id = Helper::hashDecode($id);
        }
        $model = $this->model->find($id);
        if($model != NULL) {
            $this->forgetAllCache($this->_cacheKey.$id, $this->_cacheTotalKey);
            // $this->cache()->forget($this->_cacheKey.$id);
            // $this->cache()->forget($this->_cacheTotalKey);
            return $model->delete();
        }
        return false;
    }

    /**
     *
     * This method will fetch total models
     * and will return output back to client as json
     *
     * @access public
     * @return integer
     *
     * @author Louni <sm.alikamal@hotmail.com>
     *
     **/
    public function findTotal($refresh = false) {

        $total = $this->cache()->get($this->_cacheTotalKey);
        if ($total == NULL || $refresh == true) {
            $total = $this->model->count();
            $this->cache()->forever($this->_cacheTotalKey, $total);
        }
        return $total;
    }

    public function getTranslationJson($data) {
        $language = app('language');
        $data = json_decode($data) ?: new \stdClass;

        if (isset($data->{$language})) {
            return $data->{$language};
        }
        return $data;
    }

    /**
     *
     * This method will retrive cache according to repo
     *
     * @access public
     * @return integer
     *
     * @author Louni <sm.alikamal@hotmail.com>
     *
     **/
    protected function cache() {
        if ($this->cacheTag && method_exists('Cache', 'tags')) {
            return cache()->tags($this->cacheTag);
        }
        return cache();
    }

    /**
     *
     * This method will flush cache according to repo
     *
     * @access public
     * @return integer
     *
     * @author Louni <sm.alikamal@hotmail.com>
     *
     **/
    public function flush() {
        return $this->cache()->flush();
    }

    protected function contains($str, array $arr)
    {
        foreach($arr as $value) {
            if (stripos($str, $value) !== false) {
                return $value;
            }
        }
        return false;
    }

    protected function forgetAllCache($cacheKey = '', $cacheTotalKey = '') {
        
        if($cacheKey) $this->cache()->forget($cacheKey); 
        if($cacheTotalKey) $this->cache()->forget($cacheTotalKey); 

        $checkRoute = $this->contains($cacheKey, $this->routes);

        if($checkRoute !== false) {
            $key = explode('-', $this->_cacheKey);
            array_shift($key);
            $cacheKey = implode('-', $key);
            $this->cache()->forget($cacheKey);
            $totalKey = explode('-', $this->_cacheTotalKey);
            array_shift($totalKey);
            $cacheTotalKey = implode('-', $totalKey);

            // clear cache of all routes 
            foreach ($this->routes as $key => $route) {
                $key = $route.'-'.$cacheKey;
                $this->cache()->forget($key);
                $totalKey = $route.'-'.$cacheTotalKey;
                $this->cache()->forget($totalKey);
            }

        }else {
            
            // clear cache of all routes
            foreach ($this->routes as $key => $route) {
                $cacheKey = $route.'-'.$this->_cacheKey;
                $this->cache()->forget($cacheKey);
                $cacheTotalKey = $route.'-'.$this->_cacheTotalKey;
                $this->cache()->forget($cacheTotalKey);
            }
        }

    }

    protected function setCacheKey() {
        $route = Route::getFacadeRoot()->current();
        if(!empty($route)) {
            $currentPath = $route->uri();
        }else {
            $currentPath = $this->path;
        }
        $checkRoute = $this->contains($currentPath, $this->routes);

        if($checkRoute !== false) {
            $this->_cacheKey = $checkRoute.'-'.$this->_cacheKey;
            $this->_cacheTotalKey = $checkRoute.'-'.$this->_cacheTotalKey;
        }
    }

    /**
     *
     * This method will flush cache according to repo
     *
     * @access public
     * @return hashid
     *
     * @author Louni <sm.alikamal@hotmail.com>
     *
     **/
    public function hashEncode($id = '') {

        return Helper::hashEncode($id);
    }

    /**
     *
     * This method will flush cache according to repo
     *
     * @access public
     * @return integer id
     *
     * @author Louni <sm.alikamal@hotmail.com>
     *
     **/
    public function hashDecode($id = '') {
        return Helper::hashDecode($id);
    }

}
