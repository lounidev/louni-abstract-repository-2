<?php
namespace Louni\Http;

use Illuminate\Http\Request as Base;

/**
 * App Request class for proper ssl detection
 * @author Louni <sm.alikamal@hotmail.com>
 */
class Request extends Base {

    /**
     * @return boolean
     * @author Louni <sm.alikamal@hotmail.com>
     */
    public function isSecure() {
        $isSecure = parent::isSecure();

        if($isSecure) {
            return true;
        }

        if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') {
            return true;
        } else if (!empty($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https' || !empty($_SERVER['HTTP_X_FORWARDED_SSL']) && $_SERVER['HTTP_X_FORWARDED_SSL'] == 'on') {
            return true;
        } else if (!empty($_SERVER['HTTP_CF_VISITOR']) && ($scheme = json_decode($_SERVER['HTTP_CF_VISITOR'])) && $scheme->scheme == 'https') {
            return true;
        }

        return false;
    }
}