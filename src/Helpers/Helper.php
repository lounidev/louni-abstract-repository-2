<?php

namespace Louni\Helpers;
use Hashids\Hashids;

/**
 * @author Louni <sm.alikamal@hotmail.com>
 */
class Helper {
    public static function hashEncode($id, $salt = 'laravel-project', $length = 10) {
        $hash = new Hashids(config('louniabstraction.hashid.salt'), config('louniabstraction.hashid.length'));
        return $hash->encode($id);
    }

    public static function hashDecode($id, $salt = 'laravel-project', $length = 10) {
        $hash = new Hashids(config('louniabstraction.hashid.salt'), config('louniabstraction.hashid.length'));
        return !empty($hash->decode($id))? $hash->decode($id)[0] : null;
    }   
}