<?php

namespace Louni\Providers;

use Illuminate\Support\ServiceProvider as BaseServiceProvider;
use Hashids\Hashids;
use Validator;
use Louni\Services\Validation;

class ServiceProvider extends BaseServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/config/sphere.php' => config_path('sphere.php'),
        ]);
        // Validator::resolver(function($translator, $data, $rules, $messages) {
        //     return new Validation($translator, $data, $rules, $messages);
        // });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('Hashids', function ($app) {
            return new Hashids(config('sphere.hashid.salt'), config('sphere.hashid.length'));
        });
        $this->app->singleton('language',function(){
            if (request()->has('language') && request()->get('language')) {
                return request()->get('language');
            } else if (session()->has('language') && session()->get('language')) {
                return session()->get('language');
            }
        });
    }
}
